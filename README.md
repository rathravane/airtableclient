# airtableClient
Some code for working with Airtable.com

## Features
This code is just doing some basics. Feel free to pull-request additional functionality!

## Example

	final String apiKey = settings.getString ( "apiKey" );
	final String baseName = settings.getString ( "base" );

	final AirtableClient ac = new AirtableClientFactory ()
		.withApiKey ( apiKey )
		.usingBase ( baseName )
		.build ()
	;

	for ( final AirtableRecord rec : ac.listRecords ( ac.createSelector ( "myTable" ) ) )
	{
		ac.patchRecord ( taskTable, rec.getId(), 
			new JSONObject ()
				.put ( "Notes", "test patch" )
		 );
	}
	
